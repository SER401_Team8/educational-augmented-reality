package com.example.StudyHelperAR;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import helpers.SnackbarHelper;

/**
 * AR Activity that used to display 3D image
 */
/*Portions of this page are modifications based on work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
Source: https://developers.google.com/ar/develop/java/sceneform/import-assets */
public class ARActivity extends AppCompatActivity {

    private ArFragment arFragment;
    private ImageView scanner;

    // Augmented image and its associated center pose anchor, keyed by the augmented image in
    // the database.
    private final Map<AugmentedImage, ImageNode> augmentedImageMap = new HashMap<>();

    /**
     * Create AR Fragment with scanner
     * @param savedInstanceState saved instanceof current activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        //sets to dark mode if current selection
        if (themeVal.getThemeNum() == 5) {
            setTheme(R.style.ARDarkTheme);
        }

        setContentView(R.layout.activity_main);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        scanner = findViewById(R.id.image_view_fit_to_scan);

        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);
    }

    /**
     * Sets up AugmentedImages
     * @param config
     * @param session
     * @return boolean if image database is set up
     */
    public boolean setupAugmentedImagesDb(Config config, Session session) {
        AugmentedImageDatabase augmentedImageDatabase;
        Bitmap bitmap = loadAugmentedImage();
        if (bitmap == null) {
            return false;
        }
        augmentedImageDatabase = new AugmentedImageDatabase(session);
        augmentedImageDatabase.addImage("tiger", bitmap);
        config.setAugmentedImageDatabase(augmentedImageDatabase);
        Toast toast1 = Toast.makeText(getApplicationContext(),
                "set up images", Toast.LENGTH_LONG);
        toast1.show();
        return true;
    }

    /**
     * Load Imaged into Bitmap
     * @return returns bitmap image
     */
    private Bitmap loadAugmentedImage() {
        try (InputStream is = getAssets().open("blanket.jpeg")) {
            Toast toast1 = Toast.makeText(getApplicationContext(),
                    "Loads image here", Toast.LENGTH_LONG);
            toast1.show();
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            Log.e("ImageLoad", "IO Exception", e);
        }
        return null;
    }

    /**
     * Registered with the Sceneform Scene object, this method is called at the start of each frame.
     *
     * @param frameTime - time since last frame.
     */
    private void onUpdateFrame(FrameTime frameTime) {
        Frame frame = arFragment.getArSceneView().getArFrame();

        // If there is no frame or ARCore is not tracking yet, just return.
        if (frame == null || frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }

        Collection<AugmentedImage> updatedAugmentedImages =
                frame.getUpdatedTrackables(AugmentedImage.class);

        for (AugmentedImage augmentedImage : updatedAugmentedImages) {
            switch (augmentedImage.getTrackingState()) {
                case PAUSED:
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    String text = "Detected Image " + augmentedImage.getName();
                    SnackbarHelper.getInstance().showMessage(this, text);
                    break;

                case TRACKING:
                    // Have to switch to UI Thread to update View.
                    scanner.setVisibility(View.GONE);
                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        //get the name of the image from the database, and remove the .png part
                        String split[] = augmentedImage.getName().split("\\.");
                        String tracking_name = split[0];
                        //add .sfb so that the application can render the right 3d model
                        tracking_name += ".sfb";
                        ImageNode node = new ImageNode(this,tracking_name);
                        node.setImage(augmentedImage);
                        //add it to the map
                        augmentedImageMap.put(augmentedImage, node);
                        arFragment.getArSceneView().getScene().addChild(node);
                    }
                    break;

                case STOPPED:
                    augmentedImageMap.remove(augmentedImage);
                    break;
            }
        }
    }


    /**
     * Gets reference png image from database to be added to ARCore
     * @return
     */
    public Bitmap getImage(){
        //Gets current theme value
        GlobalVariables book = (GlobalVariables) getApplicationContext();

        //make sure there us a textbook name
        if(!book.getTextbookName().equals("")) {
            String imageNum = book.getTextBook().getImageId();

            ImageAsyncTask task = new ImageAsyncTask(imageNum);
            Toast toast1 = Toast.makeText(getApplicationContext(),
                    "Loaded "+imageNum, Toast.LENGTH_LONG);
            toast1.show();

            try {
                Bitmap imageGet = task.execute().get();

                return imageGet;
            } catch (ExecutionException e) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            } catch (InterruptedException e) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            }
        }else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Unable to load data...", Toast.LENGTH_LONG);
            toast.show();
        }
        return null;
    }

    /**
     * When back button is pressed it returns user to the main activity
     */
    public void onBackPressed() {
        Intent homeActivity = new Intent(this, MainActivity.class);
        homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeActivity);
    }
}
