package com.example.StudyHelperAR;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

/**
 * Help Activity displays help information and sends email to developers if needed
 */
public class HelpActivity extends MainActivity {

    /**
     * Create help activity page
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        //sets theme to current selection
        switch(themeVal.getThemeNum()){
            case 5:
                setTheme(R.style.DarkTheme);
                break;
            case 4:
                setTheme(R.style.GreenAppTheme);
                break;
            case 3:
                setTheme(R.style.RedAppTheme);
                break;
            case 2:
                setTheme(R.style.OrangeAppTheme);
                break;
        }


        super.onCreate(savedInstanceState);
        setTitle(R.string.help);
        setupActionBar();
        setContentView(R.layout.activity_help);

        String[] helpInstruction = {"\nHow to use Study HelpAR:\n\n\t" +
                "1. Add text book name into search bar.\n\n\t" +
                "2. Select AR Scanner button.\n\n\t" +
                "3. Point displayed box at image to be scanned.\n\n\t" +
                "4. Image should automatically appear in screen.\n\n\t" +
                "5. If any problems occur try switching the \n\torientation of 2D image until 3D image appears on \n\tscreen."};

        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                helpInstruction);

//        ListView list = (ListView) findViewById(R.id.helpList);
//        list.setAdapter(adapter);
    }

    /**
     * Adds action bar for navigating between pages
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the back arrow in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * If back arrow option is selected ends activity and returns to home page
     * @param item back arrow menu item
     * @return item selected on the option menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent homeActivity = new Intent(this, MainActivity.class);
            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeActivity);
        }

        return super.onOptionsItemSelected(item);
    }
}
