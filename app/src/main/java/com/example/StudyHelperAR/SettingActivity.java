package com.example.StudyHelperAR;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

/**
 * Settings Page sets up setting page and adds option for dark theme
 */
public class SettingActivity extends MainActivity {

    /**
     * Sets up setting page
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.settings);

        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        switch(themeVal.getThemeNum()){
            case 5:
                setTheme(R.style.DarkTheme);
                break;
            case 4:
                setTheme(R.style.GreenAppTheme);
                break;
            case 3:
                setTheme(R.style.RedAppTheme);
                break;
            case 2:
                setTheme(R.style.OrangeAppTheme);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setupActionBar();

        //creates switches
        Switch nightSwitch = (Switch) findViewById(R.id.darkSwitch);
        Switch redSwitch = (Switch) findViewById(R.id.redSwitch);
        Switch yellowSwitch = (Switch) findViewById(R.id.orangeSwitch);
        Switch greenSwitch = (Switch) findViewById(R.id.greenSwitch);
        Switch blueSwitch = (Switch) findViewById(R.id.blueSwitch);

        switch(themeVal.getThemeNum()){
            case 5:
                nightSwitch.setChecked(true);
                nightSwitch.setTextColor(getColor(R.color.colorWhite));
                redSwitch.setTextColor(getColor(R.color.colorWhite));
                yellowSwitch.setTextColor(getColor(R.color.colorWhite));
                greenSwitch.setTextColor(getColor(R.color.colorWhite));
                blueSwitch.setTextColor(getColor(R.color.colorWhite));
                break;
            case 4:
                greenSwitch.setChecked(true);
                break;
            case 3:
                redSwitch.setChecked(true);
                break;
            case 2:
                yellowSwitch.setChecked(true);
                break;
            default:
                blueSwitch.setChecked(true);
        }

        nightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Night swtich button changes themeNum with swtich value
             * @param compoundButton switch button
             * @param b value of the button
             */
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && themeVal.getThemeNum() != 5) {
                    themeVal.setThemeNum(5);
                    changeTheme();
                } else {
                    themeVal.setThemeNum(0);
                   changeTheme();
                }
            }
        });

        redSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Red switch button changes themeNum with switch value
             * @param compoundButton switch button
             * @param b value of the button
             */
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && themeVal.getThemeNum() != 3) {
                    themeVal.setThemeNum(3);
                    changeTheme();
                } else {
                    themeVal.setThemeNum(0);
                    changeTheme();
                }
            }
        });

        greenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Green switch button changes themeNum with switch value
             * @param compoundButton switch button
             * @param b value of the button
             */
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && themeVal.getThemeNum() != 4) {
                    themeVal.setThemeNum(4);
                    changeTheme();
                } else {
                    themeVal.setThemeNum(0);
                    changeTheme();
                }
            }
        });

        yellowSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Yellow switch button changes themeNum with switch value
             * @param compoundButton switch button
             * @param b value of the button
             */
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && themeVal.getThemeNum() != 2) {
                    themeVal.setThemeNum(2);
                    changeTheme();
                } else {
                    themeVal.setThemeNum(0);
                    changeTheme();
                }
            }
        });

        blueSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /**
             * Blue switch button changes themeNum with switch value
             * @param compoundButton switch button
             * @param b value of the button
             */
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && themeVal.getThemeNum() != 2) {
                    themeVal.setThemeNum(0);
                    changeTheme();
                } else {
                    themeVal.setThemeNum(0);
                    changeTheme();
                }
            }
        });


    }

    /**
     * Starts a new settings activity when value is selected
     */
    public void changeTheme(){
        Intent themeIntent = new Intent(this, SettingActivity.class);
        startActivity(themeIntent);
    }


    /**
     * Adds back arrow option to action bar
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // add back arrow in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * If back arrow option is selected ends activity and returns to home page
     * @param item back arrow menu item
     * @return item selected on the option menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent homeActivity = new Intent(this, MainActivity.class);
            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeActivity);
        }

        return super.onOptionsItemSelected(item);
    }

}
