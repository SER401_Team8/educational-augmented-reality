package com.example.StudyHelperAR;


import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ModelRenderable;
import java.util.concurrent.CompletableFuture;

/**
 * Node for rendering an augmented image. The image is framed by placing the virtual picture frame
 * at the corners of the augmented image trackable.
 */

/*Portions of this page are modifications based on work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
Source: https://developers.google.com/ar/develop/java/sceneform/import-assets */
@SuppressWarnings({"AndroidApiChecker"})
public class ImageNode extends AnchorNode {

    private static final String TAG = "AugmentedImageNode";

    // The augmented image represented by this node.
    private AugmentedImage image;

    private static CompletableFuture<ModelRenderable> model;

    public ImageNode(Context context, String filename) {
        // Upon construction, start loading the models for the corners of the frame.

            model =
                    ModelRenderable.builder().setRegistryId(filename)
                            .setSource(context, Uri.parse("models/"+filename))
                            .build();

    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image.
     */
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image) {
        this.image = image;

        // If any of the models are not loaded, then recurse when all are loaded.
        if (!model.isDone() ) {
            CompletableFuture.allOf(model)
                    .thenAccept((Void aVoid) -> setImage(image))
                    .exceptionally(
                            throwable -> {
                                Log.e(TAG, "Exception loading", throwable);
                                return null;
                            });
        }

        // Set the anchor based on the center of the image.
        setAnchor(image.createAnchor(image.getCenterPose()));

        Node node = new Node();

        node.setParent(this);
        node.setRenderable(model.getNow(null));
    }

    public AugmentedImage getImage() {
        return image;
    }
}
