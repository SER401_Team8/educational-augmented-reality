package com.example.StudyHelperAR;

import android.os.AsyncTask;
import android.util.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DataAsyncTask extends AsyncTask<TextbookInfo, Void, ArrayList<TextbookInfo>> {
    static String server_out = null;
    static String temp_out = null;

    @Override
    protected ArrayList<TextbookInfo> doInBackground(TextbookInfo... arg0) {
        ArrayList<TextbookInfo> textbooks = new ArrayList<>();
        BufferedReader bufferedReader = null;

        try {
            String server_out = "";
            String temp_out = "";

            URL url = new URL("https://studyhelpartest.herokuapp.com/textbooks");

            //connects to database with GET Method
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");

            //checks for valid response
            if(connection.getResponseCode() != 200){
                throw new Exception("Failed: HTTP error: " + connection.getResponseCode());
            }


            bufferedReader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));

            JsonReader jsonReader = new JsonReader(new InputStreamReader(connection.getInputStream()));
            jsonReader.beginArray();

            while(jsonReader.hasNext()){
                String tempID = "";
                String tempName = "";
                String tempCode= "";
                String tempDesc = "";
                String tempImageID = "";

                jsonReader.beginObject();
                while(jsonReader.hasNext()){
                    String name = jsonReader.nextName();
                    if(name.equals("_id")){
                        tempID = jsonReader.nextString();
                    }else if (name.equals("name")){
                        tempName = jsonReader.nextString();
                    }else if (name.equals("code")) {
                        tempCode = jsonReader.nextString();
                    }else if (name.equals("desc")){
                        tempDesc = jsonReader.nextString();
                    }else if (name.equals("imageId")){
                        tempImageID = jsonReader.nextString();
                    }
                }

                TextbookInfo temp =
                        new TextbookInfo(tempID, tempName, tempCode, tempDesc, tempImageID);
                textbooks.add(temp);
                jsonReader.endObject();
            }

            jsonReader.endArray();
            jsonReader.close();

        } catch (MalformedURLException exception) {
        } catch (IOException exception) {
        } catch (Exception exception){
            exception.getMessage();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return textbooks;
    }

}
