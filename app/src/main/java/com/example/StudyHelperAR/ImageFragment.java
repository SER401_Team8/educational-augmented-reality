package com.example.StudyHelperAR;


import android.app.ActivityManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Session;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

import helpers.SnackbarHelper;

/**
 * Extend the ArFragment to customize the ARCore session configuration to include Augmented Images.
 */

/*Portions of this page are modifications based on work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
Source: https://developers.google.com/ar/develop/java/sceneform/import-assets */

public class ImageFragment extends ArFragment {
    private static final String TAG = "AugmentedImageFragment";

    // This is the name of the image in the sample database.  A copy of the image is in the assets
    // directory.  Opening this image on your computer is a good quick way to test the augmented image
    // matching.
    private static final String DEFAULT_IMAGE_NAME = "default.jpg";

    // This is a pre-created database containing the sample image.
    private static final String SAMPLE_IMAGE_DATABASE = "sample_database.imgdb";

    // Augmented image configuration and rendering.
    // Load a single image (true) or a pre-generated image database (false).
    private static final boolean USE_SINGLE_IMAGE = false;

    // Do a runtime check for the OpenGL level available at runtime to avoid Sceneform crashing the
    // application.
    private static final double MIN_OPENGL_VERSION = 3.0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Check for Sceneform being supported on this device.  This check will be integrated into
        // Sceneform eventually.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            SnackbarHelper.getInstance()
                    .showError(getActivity(), "Sceneform requires Android N or later");
        }

        String openGlVersionString =
                ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 or later");
            SnackbarHelper.getInstance()
                    .showError(getActivity(), "Sceneform requires OpenGL ES 3.0 or later");
        }
    }

    /**
     * Creates the view and plan discovery controller
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view the android app view
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        // Turn off the plane discovery since we're only looking for images
        getPlaneDiscoveryController().hide();
        getPlaneDiscoveryController().setInstructionView(null);
        getArSceneView().getPlaneRenderer().setEnabled(false);
        return view;
    }

    /**
     * Sets the session configuration
     * @param session current session
     * @return the configuration of the session
     */
    @Override
    protected Config getSessionConfiguration(Session session) {
        Config config = super.getSessionConfiguration(session);
        if (!setupAugmentedImageDatabase(config, session)) {
            SnackbarHelper.getInstance()
                    .showError(getActivity(), "Could not setup augmented image database");
        }
        return config;
    }

    /**
     * Sets up the image databasse
     * @param config
     * @param session
     * @return true if the image database is set up
     */
    private boolean setupAugmentedImageDatabase(Config config, Session session) {
        //gets image name from book
        GlobalVariables book = (GlobalVariables) getActivity().getApplicationContext();
        String name = book.getTextbookName();


        //creates image database
        AugmentedImageDatabase augmentedImageDatabase = new AugmentedImageDatabase(session);

        //adds in image to database
        augmentedImageDatabase.addImage(name, getImage());

//        AssetManager assetManager = getContext() != null ? getContext().getAssets() : null;
//        if (assetManager == null) {
//            Log.e(TAG, "Context is null, cannot initialize image database.");
//            return false;
//        }
//
//        // There are two ways to configure an AugmentedImageDatabase:
//        // 1. Add Bitmap to DB directly
//        // 2. Load a pre-built AugmentedImageDatabase
//        // Option 2) has
//        // * shorter setup time
//        // * doesn't require images to be packaged in apk.
//        if () {
//            Bitmap augmentedImageBitmap = loadAugmentedImageBitmap(assetManager);
//            if (augmentedImageBitmap == null) {
//                return false;
//            }
//
//           // augmentedImageDatabase = new AugmentedImageDatabase(session);
//
//            augmentedImageBitmap = getImage();
//
//            augmentedImageDatabase.addImage(DEFAULT_IMAGE_NAME, augmentedImageBitmap);
//            // If the physical size of the image is known, you can instead use:
//            //     augmentedImageDatabase.addImage("image_name", augmentedImageBitmap, widthInMeters);
//            // This will improve the initial detection speed. ARCore will still actively estimate the
//            // physical size of the image as it is viewed from multiple viewpoints.
//        } else {
//            // This is an alternative way to initialize an AugmentedImageDatabase instance,
//            // load a pre-existing augmented image database.
//            try (InputStream is = getContext().getAssets().open(SAMPLE_IMAGE_DATABASE)) {
//                augmentedImageDatabase = AugmentedImageDatabase.deserialize(session, is);
//            } catch (IOException e) {
//                Log.e(TAG, "IO exception loading augmented image database.", e);
//                return false;
//            }
//        }

        config.setAugmentedImageDatabase(augmentedImageDatabase);
        return true;
    }

    /**
     * Gets reference png image from database to be added to ARCore
     * @return Bitmap image to be added to image database
     */
    public Bitmap getImage(){

        //make sure there us a textbook name

        GlobalVariables book = (GlobalVariables) getActivity().getApplicationContext();
        if(!book.getTextbookName().equals("")) {
            String imageNum = book.getTextBook().getImageId();

            ImageAsyncTask task = new ImageAsyncTask(imageNum);

            try {
                Bitmap imageGet = task.execute().get();


                return imageGet;
            } catch (ExecutionException e) {
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            } catch (InterruptedException e) {
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            }
        }else {
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                    "Unable to load data...", Toast.LENGTH_LONG);
            toast.show();
        }
        return null;
    }

    /**
     * Loads in the bitmaps in the asset folders
     * @param assetManager
     * @return Bitmap image
     */
    private Bitmap loadAugmentedImageBitmap(AssetManager assetManager) {
        try (InputStream is = assetManager.open(DEFAULT_IMAGE_NAME)) {
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            Log.e(TAG, "IO exception loading augmented image bitmap.", e);
        }
        return null;
    }
}