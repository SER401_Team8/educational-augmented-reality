package com.example.StudyHelperAR;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Splash Screen activity for start of app
 */
public class SplashActivity extends AppCompatActivity {

    private final int WAIT_TIME = 1500; //1.5 seconds

    /**
     * displays the splash activity
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable(){
            /**
             * Allows the splash activity to display for 2 seconds before starting main activity
             */
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, WAIT_TIME);
    }
}
