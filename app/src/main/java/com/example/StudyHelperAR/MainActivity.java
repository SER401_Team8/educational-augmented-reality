package com.example.StudyHelperAR;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Copyright 2015-2016 Mitesh Pithadiya
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p>
 * Found at https://github.com/miteshpithadiya/SearchableSpinner
 */



/**
 * MainActivity controls the app. Main landing page for app.
 */

/**Portions of this page are modifications based on work created and shared by Google and used according to terms described in the Creative Commons 3.0 Attribution License.
Source: https://developers.google.com/ar/develop/java/sceneform/import-assets */


public class MainActivity extends AppCompatActivity {

    ArrayAdapter adapter;
    Button b1;
    SearchableSpinner spinner;
    Button testImage;
    String booklist[];
    String textBook;
    ArrayList<TextbookInfo> books;
    ImageView image;
    TextbookInfo currentBook;

    /**
     * Creates AR environment
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        //sets theme to current selection
        switch(themeVal.getThemeNum()){
            case 5:
                setTheme(R.style.DarkTheme);
                break;
            case 4:
                setTheme(R.style.GreenAppTheme);
                break;
            case 3:
                setTheme(R.style.RedAppTheme);
                break;
            case 2:
                setTheme(R.style.OrangeAppTheme);
                break;
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        //adds AR scanner button
        b1=(Button)findViewById(R.id.button);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAR();
            }
        });

        //add books to list
        createBookList();

        //disables AR Scanner button
        b1.setEnabled(false);

//        //image test button
//        testImage = (Button)findViewById(R.id.imageButton);
//        testImage.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                getImage();
//            }
//        });
//
        image = (ImageView)findViewById(R.id.imageView);
        image.setImageResource(R.drawable.books3);
    }

    /**
     * Creates option menu in right upper corner
     * @param menu menu to be added
     * @return true is menu is added to display
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    /**
     * Adds listener to option menu items
     * @param item option menu item selected
     * @return true if new page of selected item opened or closes app
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.settings){
            startActivity(new Intent(this, SettingActivity.class));
        }else if (item.getItemId() == R.id.help){
            startActivity(new Intent(this, HelpActivity.class));
        }else if (item.getItemId() == R.id.privacy){
            startActivity(new Intent(this, PrivacyActivity.class));
        }else if (item.getItemId() == R.id.aboutUs){
            startActivity(new Intent(this, AboutUsActivity.class));
        }else if (item.getItemId() == R.id.close) {
            AlertDialog.Builder alertCloseBuilder = new AlertDialog.Builder(this);
            alertCloseBuilder.setMessage("Are you sure you want to exit?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent mainAct = new Intent(Intent.ACTION_MAIN);
                            mainAct.addCategory(Intent.CATEGORY_HOME);
                            mainAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mainAct);
                            finish(); //close entire app
                        }
                    });

            AlertDialog alertClose = alertCloseBuilder.create();
            alertClose.show();

        }else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    /**
     * Start ARCore environment when scanner button is selected in new activity
     */
    private void startAR(){
        Intent ARActivity = new Intent(this, ARActivity.class);
        startActivity(ARActivity);
    }

    /**
     * Back button pressed on phone returns user to main app page
     */
    public void onBackPressed() {
        setContentView(R.layout.main_page);
    }

    /**
     * Creates booklist from database
     */
    public void createBookList() {
        //Gets current theme value
        GlobalVariables spin = (GlobalVariables) getApplicationContext();

        DataAsyncTask task = new DataAsyncTask();

        //creates searchable spinner
        spinner = (SearchableSpinner)findViewById(R.id.spinner);


        //gets books from database
        try {
            books = task.execute().get();
            booklist = new String[books.size()];

            if (books.size() == 0){
                Toast toast = Toast.makeText(getApplicationContext(), "Unable to load textbooks!", Toast.LENGTH_LONG);
                toast.show();
            }else {
                for (int i = 0; i < books.size(); i++) {
                    String temp = books.get(i).getName();
                    booklist[i] = temp;
                }
            }

            //creates adapter with current test string list
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                    booklist);

        } catch (ExecutionException exception){
            Toast toast = Toast.makeText(getApplicationContext(), "Please try again!", Toast.LENGTH_LONG);
            toast.show();

        } catch (InterruptedException exception){
            Toast toast = Toast.makeText(getApplicationContext(), "Please try again!", Toast.LENGTH_LONG);
            toast.show();

        }
        spinner.setAdapter(adapter);
        spinner.setTitle("Select Book Title");
        spinner.setPositiveButton("Close");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    b1.setEnabled(true);

                    TextView textView = (TextView) view;
                    textBook = textView.getText().toString();
                    spin.setTextbookName(textBook);
                for (TextbookInfo t : books) {
                    if (t.getName().equals(textBook)) {
                        spin.setTextbook(t);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast toast = Toast.makeText(getApplicationContext(), "Please select textbook!", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    /**
     * Gets the image png from the database based on the imageNum from the textbook
     */
    public void getImage(){
        if(books.size() > 0) {
            String imageNum = "";
            for (TextbookInfo t : books) {
                if (t.getName().equals(textBook)) {
                    imageNum = t.getImageId();
                }
            }

            ImageAsyncTask task = new ImageAsyncTask(imageNum);

            try {
                Bitmap imageGet = task.execute().get();
                image.setImageBitmap(imageGet);
            } catch (ExecutionException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            } catch (InterruptedException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "Unable to load image...", Toast.LENGTH_LONG);
                toast.show();
            }
        }else {
            Toast toast = Toast.makeText(getApplicationContext(), "Unable to load data...", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}