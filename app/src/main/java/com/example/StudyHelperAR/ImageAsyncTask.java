package com.example.StudyHelperAR;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {
    static String imageNum = null;

    /**
     * Gets imageNum to be used to retrieve correct image
     * @param imageNum image value
     */
    protected ImageAsyncTask(String imageNum) {
        this.imageNum = imageNum;
    }

    /**
     * Gets image from MongoDB
     * @param arg0
     * @return Bitmap of image
     */
    @Override
    protected Bitmap doInBackground(Void... arg0) {
        try {
            URL url = new URL("https://studyhelpartest.herokuapp.com/textbooks/image/"+imageNum);

            //connects to database with GET Method
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            //checks for valid response from database
            if (connection.getResponseCode() != 200) {
                throw new Exception("Failed: HTTP error: " + connection.getResponseCode());
            } else {
                return BitmapFactory.decodeStream((InputStream) connection.getContent(), null, null);
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        return null;
    }
}
