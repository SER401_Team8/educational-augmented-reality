package com.example.StudyHelperAR;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

/**
 * AboutUsActivity display information about the developer activity
 */
public class AboutUsActivity extends MainActivity {

    /**
     * Creats about us activity page
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        //sets to theme to current selection
        switch(themeVal.getThemeNum()){
            case 5:
                setTheme(R.style.DarkTheme);
                break;
            case 4:
                setTheme(R.style.GreenAppTheme);
                break;
            case 3:
                setTheme(R.style.RedAppTheme);
                break;
            case 2:
                setTheme(R.style.OrangeAppTheme);
                break;
        }

        super.onCreate(savedInstanceState);
        setTitle("About Us");
        setupActionBar();
        setContentView(R.layout.activity_about_us);
    }

    /**
     * Add back arrow to privacy action bar
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the back arrow in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * If back arrow option is selected ends activity and returns to home page
     * @param item back arrow menu item
     * @return item selected on the option menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent homeActivity = new Intent(this, MainActivity.class);
            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Send comment email to developers
     * @param view activity view
     */
    public void process(View view){
        if(view.getId() == R.id.sendEmail){
            Intent intent = new Intent(Intent.ACTION_SEND);

            intent.setData(Uri.parse("mailto:"));
            String[] to={"sachten@asu.edu", "gfraiman@asu.edu", "aralsto1@asu.edu","tmpino@asu.edu",
            "achagam@gmail.com"};
            intent.putExtra(Intent.EXTRA_EMAIL, to);
            intent.putExtra(Intent.EXTRA_SUBJECT, "HelpAR Comment");
            intent.setType("message/rfc822"); //uses email type
            Intent chooser = Intent.createChooser(intent, "Send Email");

            startActivity(chooser);
        }
    }
}
