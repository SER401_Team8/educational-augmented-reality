package com.example.StudyHelperAR;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Async Task to get the 3D object
 */
public class ObjectAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        static String imageNum = null;

        /**
         * Gets imageNum to be used to retrieve correct image
         * @param imageNum image value
         */
        protected ObjectAsyncTask(String imageNum) {
            this.imageNum = imageNum;
        }

        /**
         * Gets image from MongoDB
         * @param arg0
         * @return Bitmap of image
         */
        @Override
        protected Bitmap doInBackground(Void... arg0) {
            try {
                URL url = new URL("https://studyhelpar.herokuapp.com/textbooks/3dimage/"+imageNum);

                //connects to database with GET Method
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");

                //checks for valid response from database
                if (connection.getResponseCode() != 200) {
                    throw new Exception("Failed: HTTP error: " + connection.getResponseCode());
                } else {
                    return BitmapFactory.decodeStream((InputStream) connection.getContent(), null, null);
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }

            return null;
        }
}
