package com.example.StudyHelperAR;

public class ImageInfo {
    public String _id;
    public String path;
    public String caption;

    public ImageInfo(String _id, String path, String caption){
        this._id = _id;
        this.path = path;
        this.caption = caption;
    }

    public String get_id(){
        return _id;
    }

    public void set_id(String _id){
        this._id = _id;
    }

    public String getPath(){
        return path;
    }

    public void setPath(String path){
        this.path = path;
    }

    public String getCaption(){
        return caption;
    }

    public void setCaption(String caption){
        this.caption = caption;
    }
}
