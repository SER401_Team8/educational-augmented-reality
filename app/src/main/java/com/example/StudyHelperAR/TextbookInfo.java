package com.example.StudyHelperAR;

public class TextbookInfo {
    private String _id;
    private String name;
    private String code;
    private String desc;
    private String imageId;

    public TextbookInfo(String _id, String name, String code, String desc, String imageId){
        this._id = _id;
        this.name = name;
        this.code = code;
        this.desc = desc;
        this.imageId = imageId;
    }

    public String get_id(){
        return _id;
    }

    public void set_id(String _id){
        this._id = _id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getDesc(){
        return desc;
    }

    public void setDesc(String desc){
        this.desc = desc;
    }

    public String getImageId(){
        return imageId;
    }

    public void setImageId(String imageId){
        this.imageId = imageId;
    }
}
