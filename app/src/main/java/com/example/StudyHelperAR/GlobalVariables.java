package com.example.StudyHelperAR;

import android.app.Application;

/**
 * Keeps track of the theme number. 5 is for dark theme. 0 is for light theme.
 */
public class GlobalVariables extends Application {
    private int themeNum = 0;
    private int startSpinner = 0;
    private String textbookName = "";
    private TextbookInfo textbook;

    /**
     * Gets the theme number value
     * @return theme value number
     */
    public int getThemeNum() {
        return themeNum;
    }

    /**
     * Sets theme number value
     * @param themeNum theme number input
     */
    public void setThemeNum(int themeNum) {
        this.themeNum = themeNum;
    }

    public int getStartSpinner(){
        return startSpinner;
    }

    public void setStartSpinner(int startSpinner){
        this.startSpinner = startSpinner;
    }

    public String getTextbookName(){
        return textbookName;
    }

    public void setTextbookName(String textbookName){
        this.textbookName = textbookName;
    }

    public TextbookInfo getTextBook(){
        return textbook;
    }

    public void setTextbook(TextbookInfo textbook){
        this.textbook = textbook;
    }
}
