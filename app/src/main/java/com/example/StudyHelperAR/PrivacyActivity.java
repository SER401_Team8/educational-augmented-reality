package com.example.StudyHelperAR;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * This is class for the privacy option menu page
 */
public class PrivacyActivity extends MainActivity {

    /**
     * Sets up privacy page
     * @param savedInstanceState prior app state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Gets current theme value
        GlobalVariables themeVal = (GlobalVariables) getApplicationContext();

        //sets theme to current selection
        switch(themeVal.getThemeNum()){
            case 5:
                setTheme(R.style.DarkTheme);
                TextView textView = (TextView)findViewById(R.id.textView1);
                textView.setTextColor(getColor(R.color.colorWhite));
                break;
            case 4:
                setTheme(R.style.GreenAppTheme);
                break;
            case 3:
                setTheme(R.style.RedAppTheme);
                break;
            case 2:
                setTheme(R.style.OrangeAppTheme);
                break;
        }

        super.onCreate(savedInstanceState);
        setTitle(R.string.privacy);
        setupActionBar();
        setContentView(R.layout.activity_privacy);

        //add hyperlink to google privacy policy
        TextView textLink = (TextView)findViewById(R.id.privacyLink);
        textLink.setMovementMethod(LinkMovementMethod.getInstance());

        //adds hyperlink to arcore website
        TextView arLink = (TextView)findViewById(R.id.arLink);
        arLink.setMovementMethod(LinkMovementMethod.getInstance());

        //sets hyperlink to white when dark theme is selected
        if (themeVal.getThemeNum() == 5) {
            textLink.setLinkTextColor(getColor(R.color.colorWhite));
            arLink.setLinkTextColor(getColor(R.color.colorWhite));
        }
    }

    /**
     * Add back arrow to privacy action bar
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the back arrow in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * If back arrow option is selected ends activity and returns to home page
     * @param item back arrow menu item
     * @return item selected on the option menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent homeActivity = new Intent(this, MainActivity.class);
            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeActivity);
        }

        return super.onOptionsItemSelected(item);
    }
}
